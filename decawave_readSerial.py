import time
import serial
import sys
import array
import can
import os
import usb.core

try:
    ser = serial.Serial()
    ser.port = "/dev/ttyUSB_Decawave"
    ser.baudrate = 115200
    ser.bytesize = serial.EIGHTBITS 
    ser.parity =serial.PARITY_NONE 
    ser.stopbits = serial.STOPBITS_ONE 
    ser.timeout = 1
    ser.open()
    ser.write(b'\r\r')
    time.sleep(1)
    ser.write(b'lep\r')
    ser.close()
except Exception as e:
    print(e)
    pass

os.system("sudo slcand -o -c -s5 /dev/ttyUSB_CANable can0")
time.sleep(0.1)
os.system("sudo ifconfig can0 up")
time.sleep(0.1)

#connect to the CAN bus
bus = can.interface.Bus(bustype='socketcan', channel='can0', bitrate=250000)

def parseSerialStream(serialData):
    serialData_split = serialData.split(',')
    print(sys.getsizeof(serialData))
    print(serialData_split)
    if(sys.getsizeof(serialData) < 40):
        sendVehicleCAN(0xFF)
    else:
        decawaveType = serialData_split[0]        
        decawaveDistance = convertStringToInt(serialData_split[1])
        #Vehicle
        if (decawaveType[2] == '8' and decawaveType[3] == '6'):
            sendVehicleCAN(decawaveDistance)
        #Pedistrian
        elif (decawaveType[2] == '8' and decawaveType[3] == '0'):
            sendPedestrianCAN(decawaveDistance)
        else:
            sendVehicleCAN(0xFF)

def sendVehicleCAN(distance):
    msg = can.Message(arbitration_id=0x0A00ABAB,
                      data=[0x86, 0, 0, 0, 0, 0, 0, 0])

    #Vehcile Caution
    if(distance <= 10 and distance >= 5):
        msg = can.Message(arbitration_id=0x0A00ABAB,
                      data=[0x86, 1, 0, 0, 0, 0, 0, 0])
    #Vehcile Danger
    elif(distance < 5 ):
        msg = can.Message(arbitration_id=0x0A00ABAB,
                      data=[0x86, 2, 0, 0, 0, 0, 0, 0])
    else:
        msg = can.Message(arbitration_id=0x0A00ABAB,
                      data=[0xFF, 0, 0, 0, 0, 0, 0, 0])
    try:
        bus.send(msg)
        print("Vehcile Message sent: ", msg)
    except can.CanError:
        print("Message NOT sent")

def sendPedestrianCAN(distance):
    msg = can.Message(arbitration_id=0x0A00ABAB,
                      data=[0x80, 0, 0, 0, 0, 0, 0, 0])

    #Vehcile Caution
    if(distance <= 20 and distance >=10):
        msg = can.Message(arbitration_id=0x0A00ABAB,
                      data=[0x80, 1, 0, 0, 0, 0, 0, 0])
    #Vehcile Danger
    elif(distance < 10):
        msg = can.Message(arbitration_id=0x0A00ABAB,
                      data=[0x80, 2, 0, 0, 0, 0, 0, 0])
    else:
        msg = can.Message(arbitration_id=0x0A00ABAB,
                      data=[0xFF, 0, 0, 0, 0, 0, 0, 0])
    try:
        bus.send(msg)
        print("Pedestrian Message sent: ", msg)
    except can.CanError:
        print("Message NOT sent")

def convertStringToInt(stringData):
    count = 0
    stringArray = []
    while True:
        if stringData[count] == " ":
            stringNumber = "".join(stringArray)
            return float(stringNumber)
        
        stringArray.append(stringData[count])
        count += 1
        
ser.open()
time.sleep(0.1)

while True:
    try:
        data=str(ser.readline())
        parseSerialStream(data)
        time.sleep(0.01)
    except Exception as e:
        print(e)
        pass
    except KeyboardInterrupt:
        ser.close()